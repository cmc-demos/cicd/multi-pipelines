# multi-pipelines

an example of a very simple way to execute different jobs based on which files were changed in the commit triggering the pipeline

## Documentation

makes use of the following keywords:


https://docs.gitlab.com/ee/ci/yaml/#ruleschanges


https://docs.gitlab.com/ee/ci/yaml/#only--except
